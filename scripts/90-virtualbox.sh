set -ux

#
# packages need to install VBGA
#
echo "### 90-virtualbox.sh"
dnf -y install bzip2 elfutils-libelf-devel epel-release gcc kernel-devel-`uname -r` kernel-headers perl tar 

VBOX_VERSION=$(curl https://download.virtualbox.org/virtualbox/LATEST-STABLE.TXT)
cd /tmp
wget -q https://download.virtualbox.org/virtualbox/${VBOX_VERSION}/VBoxGuestAdditions_${VBOX_VERSION}.iso
mount -o loop VBoxGuestAdditions_${VBOX_VERSION}.iso /mnt
rm -rf /usr/sbin/vbox-uninstall-guest-additions
sh /mnt/VBoxLinuxAdditions.run install --force
umount /mnt
rm -rf VBoxGuestAdditions_*.iso
systemctl enable vboxadd.service
