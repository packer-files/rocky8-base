#!/bin/bash
### Disable SELinux
sed -i -e 's/^SELINUX=.*/SELINUX=disabled/' /etc/selinux/config
setenforce 0