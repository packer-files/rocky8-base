#!/bin/bash -eux
yum install -y dnf-plugins-core
yum install -y epel-release
yum config-manager --set-enabled powertools
yum update -y
yum install -y vim wget rsync tcl environment-modules
dnf -y install bzip2 elfutils-libelf-devel epel-release gcc kernel-devel-`uname -r` kernel-headers perl tar 
dnf -y install dkms
reboot