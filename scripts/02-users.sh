#!/bin/bash

# Create user without creating Home (-M), since it is already on the headnode
mkdir -p /nfs/home/
groupadd -g 2001 compute
echo "%compute ALL=NOPASSWD:ALL" > /etc/sudoers.d/compute
### Alice
useradd  -m -c "Alice is a compute user" -d /nfs/home/alice -u 2001 -g compute  -s /bin/bash alice
su - alice -c 'mkdir -p ~/.ssh'
su - alice -c 'curl -s https://raw.githubusercontent.com/hashicorp/vagrant/main/keys/vagrant.pub > ~/.ssh/authorized_keys'
cat << EOF > /nfs/home/alice/.ssh/config
Host *
    StrictHostKeyChecking no
    UserKnownHostsFile=/dev/null
EOF
chown alice: /nfs/home/alice/.ssh/config
ln -s /nfs/home/alice /home/alice

### Bob
useradd  -m -c "Bob is a compute user" -d /nfs/home/bob -u 2002 -g compute  -s /bin/bash bob
su - bob -c 'mkdir -p ~/.ssh'
cat << EOF > /nfs/home/bob/.ssh/config
Host *
    StrictHostKeyChecking no
    UserKnownHostsFile=/dev/null
EOF
chown bob: /nfs/home/bob/.ssh/config
ln -s /nfs/home/bob /home/