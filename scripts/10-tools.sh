#!/bin/bash -eux
yum install -y golang
export GOPATH=/go
go install github.com/benhoyt/prig@latest
ln -s /go/bin/prig /usr/local/bin/


### 
cat >> /etc/profile.d/wildq.sh << \EOF 
export PATH=${PATH}:/usr/local/bin 
EOF
yum install -y jq python3-pip python3-devel moreutils
pip3 install requests
pip3 install wildq
