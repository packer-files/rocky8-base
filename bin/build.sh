#!/bin/bash
: ${CI_PIPELINE_ID:="0"}
: ${PACKER_FORCE:="false"}
: ${CI_PROJECT_NAME:="rocky8-base"}
: ${SCRATCH_DIR:="/scratch"}
: ${SOURCE_PATH:="https://app.vagrantup.com/rockylinux/boxes/8/versions/4.0.0/providers/virtualbox.box"}

env |sort |grep -v "^_"|sed -e 's/^/export /' > env.sh

if [[ "${PACKER_FORCE}" == "true" ]]; then
    PACKER_OPTS="${PACKER_OPTS} -force"
fi

packer build ${PACKER_OPTS} -var "ci_pipeline_id=${CI_PIPELINE_ID}" \
             -var "ci_project_name=${CI_PROJECT_NAME}" \
             -var "scratch_dir=${SCRATCH_DIR}" \
             -var "source_path=${SOURCE_PATH}" packer.json
