#!/bin/bash -eux

if [[ ! -d tests/bats-assert ]];then
    mkdir -p tests/bats-assert
    cd tests/bats-assert
    wget -qO- https://github.com/bats-core/bats-assert/archive/refs/tags/v2.0.0.tar.gz |tar xfz - --strip-components=1  
fi