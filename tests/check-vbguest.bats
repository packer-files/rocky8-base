#!/usr/bin/env bats

setup() {
    load 'bats-assert/load'
}
@test "invoking lsmod within VM to check for vboxguest" {
  run vagrant ssh -c 'lsmod |grep vbox'
  assert_output --partial "vboxguest"
}
