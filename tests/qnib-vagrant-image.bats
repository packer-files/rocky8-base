#!/usr/bin/env bats

setup() {
    load 'bats-assert/load'
}
@test "look for 'rocky8-base' in /etc/qnib-vagrant-image" {
  run vagrant ssh -c 'cat /etc/qnib-vagrant-image'
  assert_output --partial "rocky8-base"
}
